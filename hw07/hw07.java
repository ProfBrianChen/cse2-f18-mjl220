//Matthew Lee
//CSE-002
//Professor Chen
//HW07
//10/30/18
//Objective: Accomplish all methods described in homework, and to successfully implement them together to analyze/edit a piece of text

import java.util.*;

public class hw07 {
    //First method to grab user's text input
    public static String sampleText() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a sample text:");
        String input = scan.nextLine();
        System.out.println("You entered: " + input);
        return input;
    }
    //2nd method to print menu
    public static String printMenu() {
        Scanner scan = new Scanner(System.in);
        String option = "x";
        while (!option.equals("c") && !option.equals("w") && !option.equals("f") && !option.equals("r") && !option.equals("s") && !option.equals("q")) {
            System.out.println("MENU\nc - Number of non-whitespaces characters\nw - Number of words\nf - Find text\nr - Replaces all !'s\ns - Shorten spaces\nq - Quit\n\nChoose an option:\n");
            option = scan.next();
        }
        return option;
    }

    public static void getNumOfNonWSCharacters(String input){
        int wscount = 0;
        for (int i = 0; i <= (input.length() -1); i++){
            if (String.valueOf(input.charAt(i)).equals(" ")){
                wscount += 1;
            }
        }
        int output = input.length() - wscount;
        System.out.println("Number of non-whitespace characters: " + output);
    }
    //3rd method to get number of words from text input
    public static void getNumOfWords(String input){
        int wscount = 1;
        //for loop to iterate through input text
        for (int i = 0; i <= input.length()- 1; i++){
            //skips index (0)
            if (i == 0) {
            }
                //if statement with counter finding (x,x-1) = (" ",character)
            else if (!String.valueOf(input.charAt(i)).equals(" ") && String.valueOf(input.charAt(i-1)).equals(" ")){ //likely source of error
                wscount += 1;
            }
        }
        System.out.print("Number of words: " + wscount);
    }
    //4th method to find inputted text in sample text
    public static void findText(String input, String textsearch){
        int instance = 0;
        //for loop to iterate through input text
        for (int i = 0; i <= input.length() - textsearch.length(); i++) {
            int j = 0;
            //for loop to check for text being searched
            for (int k = 0; k <= textsearch.length() - 1; k++){
                //if clause which counts if the observed sample text matches the search word
                if (input.charAt(i+k) == (textsearch.charAt(k))){
                    j++;
                }
            }
            //if number of times individual characters of sample text matches the length of search word, instance ++
            if (j == textsearch.length()){
                instance ++;
            }
        }
        System.out.println('"' + textsearch + '"' + " instances: " + instance);
    }
    //5th method to replace exclamation points with periods
    public static void replaceExclamation(String input){
        //create array
        char[] edited = input.toCharArray();
        //iterate through sample text
        for (int i = 0; i <= input.length() - 1; i++){
            //if character at iterated index is '!', replace with '.' into array
            if (input.charAt(i) == '!'){
                edited[i] = '.';
            }
        }
        //convert array to string
        String output = new String(edited);
        System.out.println(output);
    }
    //6th method to shorten spaces of sample text
    public static String shortenSpace(String input){
        //create blank array with #elements that matches length of sample text string
        char[] edited = new char[input.length()];
        //iterate through sample text
        for (int i = 0; i <= input.length() - 1; i++){
            //skips last index to prevent error
            if (i == input.length()-1){
            }
            //if clause to check if indexes (i, i+1) = (" ", " "), which would result in a skip
            else if (input.charAt(i) == ' ' && input.charAt(i+1) == ' ') {
            }
            //else, the character would be added to the previously blank array
            else {
                edited[i] = (input.charAt(i));
            }
        }
        //convert array back to string
        String output = new String(edited);
        return output;
    }

    public static void main(String[] args) {
        //simple implementation of all methods
        Scanner scan = new Scanner(System.in);
        String input = sampleText();
        String option = printMenu();
        if (option.equals("c")) {
            getNumOfNonWSCharacters(input);
        } else if (option.equals("w")) {
            getNumOfWords(input);
        } else if (option.equals("f")) {
            System.out.println("Enter a word or phrase to be found: ");
            String textsearch = scan.next();
            findText(input, textsearch);
        } else if (option.equals("r")) {
            replaceExclamation(input);
        } else if (option.equals("s")) {
            System.out.println(shortenSpace(input));
        } else if (option.equals("q")) {
            System.out.println("Quitting Program...");
        }
    }
}

