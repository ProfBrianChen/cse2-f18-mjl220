import java.util.*;

public class lab07 {
    public static String adjective(int random){
        switch (random) {
            case 0:
                return "heartless";
            case 1:
                return "big";
            case 2:
                return "strong";
            case 3:
                return "blue";
            case 4:
                return "insecure";
            case 5:
                return "deranged";
            case 6:
                return "ripped";
            case 7:
                return "dangerous";
            case 8:
                return "dirty";
            case 9:
                return "flirtatious";
            default:
                return "blank";
        }
    }
    public static String subject_noun(int random){
        switch (random) {
            case 0:
                return "Machine";
            case 1:
                return "Bear";
            case 2:
                return "Bull";
            case 3:
                return "Dog";
            case 4:
                return "Cat";
            case 5:
                return "Hamster";
            case 6:
                return "Puppy";
            case 7:
                return "Hobo";
            case 8:
                return "Fly";
            case 9:
                return "Human";
            default:
                return "Poop";
        }
    }
    public static String verb(int random){
        switch (random) {
            case 0:
                return "trashed";
            case 1:
                return "ate";
            case 2:
                return "cooked";
            case 3:
                return "sacrificed";
            case 4:
                return "burned";
            case 5:
                return "destroyed";
            case 6:
                return "slammed";
            case 7:
                return "stamped";
            case 8:
                return "kept";
            case 9:
                return "flexed";
            default:
                return "blank";
        }
    }
    public static String object_noun(int random){
        switch (random) {
            case 0:
                return "dirt";
            case 1:
                return "pig";
            case 2:
                return "bacon";
            case 3:
                return "pizza";
            case 4:
                return "brownie";
            case 5:
                return "meat";
            case 6:
                return "tomato";
            case 7:
                return "potato";
            case 8:
                return "wood";
            case 9:
                return "metal";
            default:
                return "blank";
        }
    }
    public static String sentence1(String adj, String subject_noun, String verb, String object_noun){
        return ("The " + adj + " " + subject_noun + " " + verb + " the " + object_noun + ".");
    }
    public static String sentence2(String subject_noun, String verb, String object_noun){
        return ("The " + subject_noun + " " + verb + " the " + object_noun + ", which was rad.");
    }
    public static String sentence3(String subject_noun, String verb, String object_noun){
        return ("That " + subject_noun + " " + verb + " the heck out of the " + object_noun);
    }


    public static void main(String[] args) {
        Scanner myscanner = new Scanner(System.in);
        Random randomGenerator = new Random();
        int randomInt;
        int i = 1;
        while (i == 1) {
            randomInt = randomGenerator.nextInt(10);
            String adj = adjective(randomInt);
            randomInt = randomGenerator.nextInt(10);
            String verb = verb(randomInt);
            randomInt = randomGenerator.nextInt(10);
            String subject_noun = subject_noun(randomInt);
            randomInt = randomGenerator.nextInt(10);
            String object_noun = object_noun(randomInt);
//            System.out.println("The " + adj + " " + subject_noun + " " + verb + " the " + object_noun + ".");
            System.out.println(sentence1(adj, subject_noun, verb, object_noun));

            randomInt = randomGenerator.nextInt(10);
            verb = verb(randomInt);
            randomInt = randomGenerator.nextInt(10);
            object_noun = object_noun(randomInt);

            System.out.println(sentence2(subject_noun, verb, object_noun));

            randomInt = randomGenerator.nextInt(10);
            verb = verb(randomInt);
            randomInt = randomGenerator.nextInt(10);
            object_noun = object_noun(randomInt);

            System.out.println(sentence3(subject_noun, verb, object_noun));
            System.out.println("Type 1 to create another sentence");
            i = myscanner.nextInt();
        }
    }
}
