//Matthew Lee
//Lab 09
public class lab09 {

    public static int[] copy(int[] array){
        int[] output = new int[array.length];
        for (int i = 0; i < array.length; i++){
            output[i] = array[i];
        }
        return output;
    }
    public static int[] inverse(int[] array){
        int[] output = new int[array.length];
        for (int i = 0; i < array.length; i++){
            output[i] = array[array.length-1-i];
        }
        return output;
    }
    public static int[] inverse2(int[] array){
        int [] output = copy(array);
        output = inverse(output);
        return output;
    }
    public static void print(int[] array){
        System.out.print("[");
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
        System.out.print("]");
    }
    public static void main(String[] args) {
        int[] array0= new int[]{ 1,2,3,4,5,6,7,8,9,10 };
        int[] array1 = copy(array0);
        int[] array2;
        print(inverse(array0));
        System.out.println();
        print(inverse2(array1));
        System.out.println();
        array2 = inverse(array0);
        int[] array3 = inverse2(array2);
        print((array3));

    }
}
