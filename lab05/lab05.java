//Matthew Lee
//cse002
//lab05

import java.util.Scanner;	//import
import java.lang.*;

//this program takes a set of inputs with preference for var types inputted, depending on the variable. The two var types are ints and strings.
public class lab05 {
    public static int CN;
    public static int num_students;
    public static int Classperweek;
    public static void main(String[] args) {
       //set scanner
        Scanner myscanner;
        myscanner = new Scanner ( System.in ); //set scanner
        System.out.println("Enter course number: ");
        //set booleans to catch wrong var type inputs
        boolean correctInt = false;
        boolean correct;
        //start of while block to check for correct var types 
        while (correctInt == false) {
            correct = myscanner.hasNextInt();
            if (correct== true) {
                break;
            }
            else {
                System.out.println("Enter course number: ");
                myscanner.next();
            }
            correctInt = myscanner.hasNextInt();
        }
        CN = myscanner.nextInt();
        //repeat pattern but for different var. 
        System.out.println("Enter Number of times it meets in a week: ");
        correctInt = myscanner.hasNextInt();

        while (correctInt ==false) {
            correct = myscanner.hasNextInt();
            if (correct == true) {
                break;
            } else {
                System.out.println("Enter Number of times it meets in a week: ");
                myscanner.next();
            }
            correctInt = myscanner.hasNextInt();
        }
        Classperweek = myscanner.nextInt();
        //repeat pattern but for different var. 
        System.out.println("Enter Number of Students");
        correctInt = myscanner.hasNextInt();
        while (correctInt == false) {
            correct = myscanner.hasNextInt();
            if (correct == true) {
                break;
            } else {
                System.out.println("Enter Number of Students");
                myscanner.next();
            }
            correctInt = myscanner.hasNextInt();
        }
        num_students = myscanner.nextInt();
        //inputting of string variables (no need for checking var since str takes anything)
        System.out.println("Enter department name: ");
        String dept = myscanner.next();
        System.out.println(" ");

        System.out.println("Enter Time the Class Starts: ");
        String StartTime = myscanner.next();

        System.out.println("Enter Instructor Name: ");
        String Instructor = myscanner.next();

        System.out.println("Course Number:" + CN + "\nDepartment Name:" + dept + "\nNumber of Classes a week: " + Classperweek + "\nStart Time: " + StartTime + "\nInstructor Name: " + Instructor + "\nNumber of students: " + num_students);
    }
}
