public class WelcomeClass{
  
  public static void main(String args[]){
    String str1 = " / \\/ \\/ \\/ \\/ \\/ \\ ";
    String str2 = " \\ /\\ /\\ /\\ /\\ /\\ / ";
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(str1);
    System.out.println("<-M--J--L--2--2--0->");
    System.out.println(str2);
    System.out.println("  v  v  v  v  v  v");
  } 
}