import java.util.Scanner;


public class Pyramid {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in ); //initialize scanner
        System.out.print("The square side of the pyramid is (input length): "); //Length input
        double length = myScanner.nextDouble(); //Declare Length variable from input
        System.out.print("The height of the pyramid is (input height): "); //Height input
        double height = myScanner.nextDouble(); //Declare Height variable from input
        double volume = length * length * height/3; //Calculate volume for pyramid
        System.out.printf("The volume inside the pyramid is: %.0f", volume); //System output
    }
}