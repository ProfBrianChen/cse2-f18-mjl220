import java.util.Scanner;


public class Convert {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in ); //Initialize scanner
        System.out.print("Enter the affected area in acres: "); //Input area affected by rainfall
        double Acres_Affected = myScanner.nextDouble(); //Declare Acres_Affected variable
        System.out.print("Enter the inches rainfall in the affected area: "); //Input inches of rainfall
        double Rainfall = myScanner.nextDouble(); //Declare Rainfall variable
        double Cubicmiles = 9.08169e-13 *27154* Acres_Affected * Rainfall; //Calculate conversion from Acres*inches to gallons, and gallons to cubic miles
        System.out.printf("%.8f cubic miles", Cubicmiles); //System output for calculation of Cubicmiles of rainfall
    }
}
