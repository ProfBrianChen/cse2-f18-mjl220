//Matthew Lee
//CSE-002
//Professor Chen
//HW06
//10/23/18
//Objective: Takes in integer values between (0,100], validates the input requirement, and then prints out an inversed x according to the size inputted by user.

import java.util.Scanner;

public class EncryptedX {
    public static int size;
    public static void main(String[] args) {
        //set scanner
        Scanner scan;
        scan = new Scanner ( System.in ); //set scanner
        //set booleans
        boolean bool = true;
        boolean type;
        //while loop to catch incorrect type variable or value outside of wanted parameter
        while (bool) {
            System.out.println("Enter an integer between (0,100]");
            type = scan.hasNextInt();
            if (type) {
                size = scan.nextInt();
                if (type && size>= 1 && size<= 100) {
                    break;
                }
            }
            else {
                scan.next();
            }
        }
      //for loop to print out the encrypted x according to the size inputted 
        for (int numrows = 0; numrows <= size ; numrows++){
            for (int temp = 0; temp <= size; temp++) {
                if (temp == numrows|| (temp == (size-numrows))){
                    System.out.print(" ");
                }
                else {
                    System.out.print("x");
                }
            }
            System.out.print("\n");
        }
    }
}
