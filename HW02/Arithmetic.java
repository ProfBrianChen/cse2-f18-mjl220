

public class Arithmetic {

    public static void main(String[] args) {
//Number of pairs of pants
            int numPants=3;

//Cost per pair of pants
            double pantsPrice=34.98;

//Number of sweatshirts
            int numShirts=2;

//Cost per shirt
            double shirtPrice=24.99;

//Number of belts
            int numBelts=1;

//cost per belt
            double beltCost=33.99;

//the tax rate
            double paSalesTax=0.06;

//total cost of pants
            double TotalCost_pants=numPants*pantsPrice;

//total cost of sweatshirts
            double TotalCost_shirt=numShirts*shirtPrice;

//total cost of belts
            double TotalCost_belts=numBelts*beltCost;

//sales tax on pants
            double SalesTax_pants=TotalCost_pants*paSalesTax;

//sales tax on sweatshirts
            double SalesTax_shirt=TotalCost_shirt*paSalesTax;

//sales tax on belts
            double SalesTax_belts=TotalCost_belts*paSalesTax;

//total pre-tax cost of purchase
            double PreTax_cost=TotalCost_pants+TotalCost_shirt+TotalCost_belts;

//total sales tax
            double TotalSalesTax=SalesTax_pants+SalesTax_shirt+SalesTax_belts;

//Total Transaction Cost
            double Total=PreTax_cost+TotalSalesTax;

//Print Statements to display all the previously calculated costs, taxes, and totals
            System.out.println("Matthew's Struggling Retail Store (For Lease)");
            System.out.println("Quantity\tItem\t\t\tcost\t\t\tTotal Cost\t\tTotal Sales Tax");
            System.out.printf(numShirts + "\t\t" + "SweatShirts" +"\t\t" + shirtPrice +"\t\t\t" +TotalCost_shirt + "\t\t\t" + "%.2f", SalesTax_shirt);
            System.out.printf("\n" + numBelts + "\t\t" + "Belt" +"\t\t\t" + beltCost +"\t\t\t" +TotalCost_belts + "\t\t\t" + "%.2f", SalesTax_belts);
            System.out.printf("\n" + numPants + "\t\t" + "Pants" +"\t\t\t" + pantsPrice +"\t\t\t" +TotalCost_pants + "\t\t\t" + "%.2f", SalesTax_pants);
            System.out.printf("\n\n" + "Pre-Tax Cost of Purchase:\t\t" + "%.2f", PreTax_cost);
            System.out.printf("\n"+ "Sales Tax Total:\t\t\t" + "%.2f", TotalSalesTax);
            System.out.printf("\n" + "Grand Total:\t\t\t\t" + "%.2f", Total);
    }
}
