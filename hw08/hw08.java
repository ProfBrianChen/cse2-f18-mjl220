//Matthew Lee
//CSE-002-311
//Professor Chen
//11/13/18
//Objective: Manipulate arrays using various methods.
import java.util.*;

public class hw08{
    public static void printArray(String[] cards){
        for (int i =0; i<=cards.length-1; i++){ //for loop that prints out each card
            System.out.print(cards[i] + " ");
        }
        System.out.println();
    }
    public static String[] shuffle(String[] cards){
        Random random= new Random(); //initialize random
        for (int i = 0; i<=cards.length-1; i++){//go through all cards
            int newposition = random.nextInt(cards.length-1);//sets index of new location of card
            String temp = cards[i];//set temp value to for loop value to hold lost position
            cards[i] = cards[newposition];//set current index of i to the new, random position
            cards[newposition]= temp;//swaps values back
        }
        System.out.println("Shuffled");
        return cards;
    }
    public static String[] getHand(String[] cards, int index, int numCards){
        String[] output = new String[numCards];
        if (index <numCards){//if statement to catch when there's no more cards in the deck to choose from
            System.out.println("Out of Cards. Assigning new deck...");
            output[0] = "0";
            return output;
        }
        for (int i = index; i>index-numCards; i--){
            output[index-i] = cards[i]; //assign the output hand
        }
        System.out.println();
        return output;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"};
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
        String[] cards = new String[52];
        String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
            System.out.print(cards[i]+" ");
        }
        System.out.println();
//        printArray(cards);
        cards = shuffle(cards);
        printArray(cards);
        while(again == 1){
            hand = getHand(cards,index,numCards);
            if (hand[0].equals("0")){//utilizes if statement from getHands method to assign new deck when triggered.
                index = 51;
                cards = shuffle(cards);
                continue;//jumps back to top of the while loop
            }
            printArray(hand);
            index -= numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }
}