//Matthew Lee
//CSE002-311
//Professor Chen
//11-27-18
//HW09 part 1 of 2
//Objective: use numerous array searching methods to comply to professor's demands for doing binary search and linear search
import java.util.*;

//CSE2Linear.java
public class CSE2Linear {

    public static void binarysearch(int[] input, int search){
        int lowerindex = 0;
        int upperindex = 14;
        int mid;
        int iter = 0;
        while (true){//infinite while loop broken out once it finds search input or gives up
            iter++;
            mid = (int) (lowerindex + upperindex)/2;
            if (lowerindex == mid || upperindex == mid){//if block when it gives up
                System.out.print(search + " was not found with " + iter + " iterations.");
                break;
            }
            if (search == input[mid]){//if block for when it finds value
                System.out.println(search + " was found with " + iter + " iterations.");
                break;
            }
            if (search > input[mid]){//search value is higher than middle value and moves up lower boundary to mid val
                lowerindex = mid;
            }
            if (search < input[mid]){//search value is lower than middle value and moves down upper boundary to mid val
                upperindex = mid;
            }
        }
    }
    public static int[] scramble(int[] input){//code used from my submission of hw08
        Random random= new Random(); //initialize random
        for (int i = 0; i<=input.length-1; i++){//go through all values of list
            int newposition = random.nextInt(input.length-1);//sets index of new location of next element
            int temp = input[i];//set temp value to for loop value to hold lost position
            input[i] = input[newposition];//set current index of i to the new, random position
            input[newposition]= temp;//swaps values back
        }
        return input;
    }
    public static void linearsearch(int [] input, int search){
        for (int i = 0; i <15; i++){
            if(search == input[i]){//if element of index i is value you're searching
                i++;
                System.out.println(search + " was found with "  + i + " iterations.");
                break;//complete method
            }
            if (i == 14){//if index hits the end of the loop, gives up
                System.out.println(search + " was not found with 15 iterations.");
            }
        }
    }
//  Print the final input array.  Next, prompt the user to enter a grade to be searched for.
    //Use binary search to find the entered grade. Indicate if the grade was found or not, and print out the number of iterations used.
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] input = new int[15];
        for (int i = 0; i < 15; i++) {//for loop to set all elements of list
            System.out.println("Index[" + i +"]: Enter an integer greater than last one you put and is between 0-100");
            if(!scan.hasNextInt()){//if not an int entered, requests again and skips back to up to loop
                System.out.println("Must enter Int");
                scan.next();
                i--;
                continue;
            }
            input[i] = scan.nextInt();//if scan input is int, then it assigns

            if (input[i] > 100 || input[i] < 0) {//checks input[i] to make sure it's in range of 0-100
                System.out.println("Value out of range");
                i--;
                continue;
            }
            if (i != 0 && input[i] < input[i - 1]) {//make sure input of next element is larger than last, but will skip over when i = 0 since no preceding index
                System.out.println("Next value must be greater than or equal to value of last index");
                i--;
                continue;
            }
        }
        System.out.print("\n\nInputted Array:");
        for (int i = 0; i < 15; i++) {//prints out array
        System.out.print(input[i] + " ");
        }
        //Search Part
        System.out.print("\nEnter a grade to search for: ");
        int search = scan.nextInt();
        binarysearch(input, search);
        input = scramble(input);
        System.out.println("Array has been scrambled");
        for (int i = 0; i < 15; i++) {
            System.out.print(input[i] + " ");
        }
        System.out.print("\nEnter a grade to search for: ");
        search = scan.nextInt();
        linearsearch(input, search);
    }
}
