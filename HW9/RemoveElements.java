//Matthew Lee
//CSE002-311
//Professor Chen
//11-27-18
//HW09 part 2 of 2
//Objective: use numerous array manipulating methods to comply to professor's demands for Remove, delete, and randomInput
import java.util.*;

public class RemoveElements{

    //Random input method
    public static int[] randomInput() {
        Random random = new Random();//set random method
        int output[] = new int[10];
        int input;
        for (int i = 0; i < 10; i++) {//for loop to assign array with random ints between 0 and 9
            input = random.nextInt(10);
            output[i] = input;
        }
        return output;
    }

    //given code
    public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }

    //delete method
    public static int[] delete (int[] list, int pos){
        int output[]= new int[list.length-1];//initialize output array minus 1 length due to deleted element
        int skip = 0;
        for (int i = 0; i<output.length; i++){//for loop iterating through length of output and assigning list[i] to output[i-skip]
            if (i == pos){//if desired position is hit by iter, it will skip adding and will set skip to 1
                skip = 1;
                continue;
            }
            output[i-skip]= list[i]; //sets each value of list[i] to output. output[i-skip] so that when desired delete element is passed, it will not skip the index
        }
        return output;
    }
    //remove method
    public static int[] remove(int[] list, int target){
        int count=0;
        for (int i = 0 ; i<list.length; i++){//first for loop to see how many target values in list input
            if (target == list[i]){
                count++;
            }
        }
        int output[] = new int[list.length-count];//uses count from past for loop to assign output size to list minus count
        int skip = 0;
        for (int i = 0; i<list.length; i++){//iterating through length of list
            if (target == list[i]){//if target value is element of iteration, will do skip++ and skip through rest of for loop
                skip++;
                continue;
            }
            output[i-skip] = list[i]; //like in delete method, this output[i-skip] is to make sure that for loop doesn't skip over whichever indexes that target value was skipped over
        }
        return output;
    }

    public static void main (String[]args) {
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }
}
