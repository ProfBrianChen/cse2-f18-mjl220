import java.lang.*;

//My program assigns a random number between 0 and 51, and then breaks off into suits depending on the number. After, it executes a modulo and if the remainder is larger than 10, then it
// breaks it up into the royal cards from 1 to 4. If it's not over 10, then i will use it's remainder value as the card number, then prints out the result at the last line.
public class CardGenerator {

    public static void main(String[] args) {

            int card = (int) (Math.random() * (51));
            String suit = "null";
            if (card < 13) { //diamonds
                suit = "diamonds";
            }
            else if (card > 12 && card < 25) { //clubs
                suit = "clubs";
            }
            else if (card > 24 && card < 39) { //hearts
                suit = "hearts";
            } else { //spades
                suit = "spades";
            }
            card = card % 13 + 1;
            if (card > 10) {
                card = card - 10;
                if (card == 1) {
                    System.out.println("You have a Jack of " + suit);
                }
                if (card == 2) {
                    System.out.println("You have a Queen of " + suit);
                }
                if (card == 3) {
                    System.out.println("You have a King of " + suit);
                }
                if (card == 4) {
                    System.out.println("You have an Ace of " + suit);
                }
            } else
                System.out.println("You have a " + card + " of " + suit);
    }
}
