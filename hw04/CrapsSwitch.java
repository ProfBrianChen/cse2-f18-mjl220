//Matthew Lee
//CSE002
//9/25/18
//Professor Chen
//Purpose of this program is to use SWITCH statements to determine the name of the pair for the given values of the 2 dice (whether it be manual or random) 

import java.util.Scanner;	//import
import java.lang.*;

//CrapsSwitch.java
public class CrapsSwitch {
// declare variables as public
    public static int dice1;
    public static int dice2;
    public static void main(String[] args) {
        Scanner myscanner; //initialize scanner
        myscanner = new Scanner ( System.in ); //set scanner
        System.out.println("Type '1' if you would like to set the dice manually: ");//Input to check if user wants manual input
        int choice = myscanner.nextInt();
      //Choice Switch block  
      switch (choice) {
            case 1:
                System.out.println("State first dice value [1-6]: ");
                dice1 = myscanner.nextInt();
                //while block to catch invalid input values for dice
                while (dice1 > 6 || dice1 < 1) {
                    System.out.println("Error: Value out of range [1-6]");
                    System.out.println("State first dice value [1-6]: ");
                    dice1 = myscanner.nextInt();
                }
          
                //while block to catch invalid input values for dice
                System.out.println("State second dice value [1-6]: ");
                dice2 = myscanner.nextInt();
                while (dice2 > 6 || dice2 < 1) {
                    System.out.println("Error: Value out of range [1-6]");
                    System.out.println("State second dice value [1-6]: ");
                    dice2 = myscanner.nextInt();
                }
                break;
          //Case block to if user choose to have random values
            default:
                dice1 = (int) (Math.random() * (6) + 1);
                dice2 = (int) (Math.random() * (6) + 1);
                System.out.println(dice1 + " " + dice2);
                break;
        }
        int sum = dice1 + dice2;//declare and set sum
        String output1 = " ";
      //switch block to get the preliminary output string based off sum 
        switch (sum) {
            case 2:
                output1 = "Snake Eyes";
                System.out.println(output1);
                return;
            case 3:
                output1 = "Ace Deuce";
                System.out.println(output1);
                return;
            case 4:
                output1 = "Four";
                break;
            case 5:
                output1 = "Fever Five";
                System.out.println(output1);
                return;
            case 6:
                output1 = "six";
                break;
            case 7:
                output1 = "Seven Out";
                System.out.println(output1);
                return;
            case 8:
                output1 = "eight";
                break;
            case 9:
                output1 = "Nine";
                break;
            case 10:
                output1 = "Ten";
                break;
            case 11:
                output1 = "Yo-leven";
                System.out.println(output1);
                return;
            case 12:
                output1 = "Boxcars";
                System.out.println(output1);
                return;
            default: break;
        }
      //start of getting output2 string based off each dice values
        String output2 = " ";
        switch (dice1){
            case 1: switch (dice2) {
                case 3:
                    output2 = "Easy";
                    break;
                case 5:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
            break;
            case 2: switch (dice2) {
                case 2:
                    output2 = "hard";
                    break;
                case 4: case 6:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 3: switch (dice2) {
                case 3:
                    output2 = "Hard";
                    break;
                case 5:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 4: switch (dice2) {
                case 4:
                    output2 = "Hard";
                    break;
                case 6:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 5: switch (dice2){
                case 5:
                    output2 = "Hard";
                    break;
                default:
                    break;
            }
                break;
        }
        switch (dice2){
            case 1: switch (dice1) {
                case 3: case 5:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 2: switch (dice1) {
                case 2:
                    output2 = "hard";
                    break;
                case 4: case 6:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 3: switch (dice1) {
                case 3:
                    output2 = "Hard";
                    break;
                case 5:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 4: switch (dice1) {
                case 4:
                    output2 = "Hard";
                    break;
                case 6:
                    output2 = "Easy";
                    break;
                default:
                    break;
            }
                break;
            case 5: switch (dice1){
                case 5:
                    output2 = "Hard";
                    break;
                default:
                    break;
            }
                break;
        }
      //output statement
        System.out.println(output2 + " " + output1);

    }
}
