//Matthew Lee
//CSE002
//9/25/18
//Professor Chen
//Purpose of this program is to use IF statements to determine the name of the pair for the given values of the 2 dice (whether it be manual or random) 

import java.util.Scanner;	//import
import java.lang.*;

public class CrapsIf {
    //Declare Variables as public
    public static int dice1;
    public static int dice2;
    public static void main(String[] args) {
        Scanner myscanner; //initialize scanner
        myscanner = new Scanner ( System.in ); //set scanner
        System.out.println("Type '1' if you would like to set the dice manually: "); //Input to check if user wants manual input
        int choice = myscanner.nextInt();
      //Choice if block  
      if (choice == 1) {
            System.out.println("State first dice value [1-6]: ");
            dice1 = myscanner.nextInt();
        //While block to catch invalid values 
            while (dice1 > 6 || dice1 < 1) {
                System.out.println("Error: Value out of range [1-6]");
                System.out.println("State first dice value [1-6]: ");
                dice1 = myscanner.nextInt();
            }
            System.out.println("State second dice value [1-6]: ");
            dice2 = myscanner.nextInt();
            while (dice2 > 6 || dice2 < 1) {
                System.out.println("Error: Value out of range [1-6]");
                System.out.println("State second dice value [1-6]: ");
                dice2 = myscanner.nextInt();
            }
        }
      //else block if user doesn't opt for manual input
        else {
            dice1 = (int) (Math.random() * (6) +1);
            dice2 = (int) (Math.random() * (6) +1);
        }
        int sum = dice1 + dice2; //declare and set sum var
      //start of the if block to determine which pair they are
      if (dice1 == 1 || dice2 ==1) {
            if (dice1 == 2 || dice2 == 2) {
                System.out.println("Ace Deuce");
            }
            else if (dice1 == 3 || dice2 == 3) {
                System.out.println("Easy Four");
            }
        }
        if (sum == 2){
            System.out.println("Snake Eyes");
        }
        if (dice1 == 2 && dice2 == 2){
            System.out.println("Hard Four");
        }
        if (sum == 5){
            System.out.println("Fever Five");
        }
        if (dice1 == 3 && dice2 == 3){
            System.out.println("Hard Six");
        }
        else if (sum == 6){
            System.out.println("Easy Six");
        }
        if (sum == 7){
            System.out.println("Seven Out");
        }
        if (dice1 == 4 && dice2 == 4){
            System.out.println("Hard Eight");
        }
        else if (sum == 8){
            System.out.println("Easy Eight");
        }
        if (sum == 9) {
            System.out.println("Nine");
        }
        if (dice1 == 5 && dice2 == 5){
            System.out.println("Hard Ten");
        }
        else if (sum == 10){
            System.out.println("Easy Ten");
        }
        if (sum == 11){
            System.out.println("Yo-leven");
        }
        if (sum == 12){
            System.out.println("Boxcars");
        }
    }
}
