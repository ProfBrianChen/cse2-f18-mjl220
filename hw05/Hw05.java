//Matthew Lee
//cse002
//hw05
import java.util.Scanner;
import java.lang.*;

public class Hw05 {
    public static int hands;
    public static float p1;
    public static float p2;
    public static float p3;
    public static float p4;
    public static boolean pair_count;
    public static boolean trip_count;
    public static boolean dub_count;
    public static boolean quad_count;

    public static void main(String[] args) {
        //set scanner: scan
        Scanner scan;
        scan = new Scanner(System.in);

        //initialize booleans
        boolean correctInt = false;
        boolean correct;

        System.out.println("How many hands would you like to generate? : ");
        //while loop to catch non-int inputs
        while (!correctInt) {
            correct = scan.hasNextInt();
            if (correct) {
                break;
            } else {
                System.out.println("How many hands would you like to generate? : ");
                scan.next();
            }
            correctInt = scan.hasNextInt();
        }
        hands = scan.nextInt();

        //while block to simulate number of hands desired
        int i = 1;
        while (i < hands) {
            //first initializing of card numbers
            int c1 = (int) (Math.random() * (51));
            int c2 = (int) (Math.random() * (51));
            int c3 = (int) (Math.random() * (51));
            int c4 = (int) (Math.random() * (51));
            int c5 = (int) (Math.random() * (51));
            while (c1 == c2 || c1 == c3 || c1 == c4 || c1 == c5 || c2 == c3 || c2 == c4 || c2 == c5 || c3 == c4 || c3 == c5 || c4 == c5) {
                c1 = (int) (Math.random() * (51));
                c2 = (int) (Math.random() * (51));
                c3 = (int) (Math.random() * (51));
                c4 = (int) (Math.random() * (51));
                c5 = (int) (Math.random() * (51));
            } //while loop to catch duplicates
            //find the core value of each card
            c1 = c1 % 13 + 1;
            c2 = c2 % 13 + 1;
            c3 = c3 % 13 + 1;
            c4 = c4 % 13 + 1;
            c5 = c5 % 13 + 1;

            pair_count = false;
            trip_count = false;
            dub_count = false;
            quad_count = false;
            //if blocks to find if hand has either single or double pair
            if (c1 == c2 || c1 == c3 || c1 == c4 || c1 == c5 || c2 == c3 || c2 == c4 || c2 == c5 || c3 == c4 || c3 == c5 || c4 == c5) {
                pair_count = true;
                if (c1 == c2 || c1 == c3 || c1 == c4 || c1 == c5 ) {
                    if (c1 != c2) {
                        if (c2 == c3 || c2 == c4 || c2 == c5) {
                            dub_count = true;
                        }
                    }if (c1 != c3) {
                        if (c3 == c4 || c3 == c5) {
                            dub_count = true;
                        }
                    }if (c1 != c4) {
                        if (c4 == c5) {
                            dub_count = true;
                        }
                    }
                }
                if (c2 == c3 || c2 == c4 || c2 == c5) {
                    if (c2 != c3) {
                        if (c3 == c4 || c3 == c5) {
                            dub_count = true;
                        }
                    }if (c2 != c4) {
                        if (c4 == c5) {
                            dub_count = true;
                        }
                    }
                }

            }

            //if statement to find if hand has triple
            if (c1 == c2 && c1 == c3 || c1 == c2 && c1 == c4 || c1 == c2 && c1 == c5 || c1 == c3 && c1 == c4 || c1 == c3 && c1 == c5 || c2 == c3 && c2 == c4 || c2 == c3 && c2 == c5 || c2 == c4 && c2 == c5 || c3 == c4 && c3 == c5) {
                trip_count = true;
            }
            //if statement to find it hand has quad. After, it goes into nested if statements from strongest to weakest hand, for given circumstances.
            if (c1 == c2 && c1 == c3 && c1 == c4 || c1 == c2 && c1 == c3 && c1 == c5 || c1 == c2 && c1 == c5 && c1 == c4  || c1 == c5 && c1 == c3 && c1 == c4 || c2 == c3 && c2 == c4 && c2 == c5) {
                ++p4;
            }
            else{
                if (trip_count) {
                    ++p3;
                } else {
                    if (dub_count) {
                        ++p2;
                    } else {
                        if (pair_count) {
                            ++p1;
                        }
                    }
                }
            }
        i++; //increment var for while loop
        }
        //divide count of hand types by hands to find probability
        p1 /= hands;
        p2 /= hands;
        p3 /= hands;
        p4 /= hands;

        System.out.println("The number of loops: " + hands);
        System.out.printf("\nThe probability of Four-of-a-kind: %.3f",(p4));
        System.out.printf("\nThe probability of Three-of-a-kind : %.3f",(p3));
        System.out.printf("\nThe probability of Two-pair: %.3f",(p2));
        System.out.printf("\nThe probability of One-pair: %.3f", (p1));

    }
}
