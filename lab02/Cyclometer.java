public class Cyclometer {
    	// main method required for every Java program

  
  
  
  public static void main(String[] args) {
  // our input data. Document your variables by placing your

    int secsTrip1=480;  //Seconds for trip 1
    int secsTrip2=3220;  //Seconds for trip 2
		int countsTrip1=1561;  //Rotations for Trip 1
		int countsTrip2=9037; //Rotations for Trip 2
    double wheelDiameter=27.0;  //Diamater for the Wheel
    double PI=3.14159; //Constant Pi
    double feetPerMile=5280;  //Conversion of mile to feet
    double inchesPerFoot=12;   //Convrsion from foot to inches
    double secondsPerMinute=60;  //Conversion from minutes to seconds
	  double distanceTrip1, distanceTrip2,totalDistance;  //Declaring the currently unassigned variables 
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
