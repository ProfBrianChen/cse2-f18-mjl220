//Provide inputs of the original cost of check, percentage of tip, number of people to split upon.
//Spits out output of the cost each person must pay

import java.util.Scanner;	//import

public class Check {
    public static void main(String[] args) {
        Scanner myscanner; //declare total input
        //Scanner tippercent; //declare tip percent input
        //Scanner num_people; //declare the number of people to split among

     
        myscanner = new Scanner ( System.in ); //set scanner
        System.out.println("How much is the bill?"); //input for bill cost
        double total = myscanner.nextDouble(); //declare total
        System.out.println("What percent are you going to tip? (Whole number)"); //input for percent tip 
        double tippercent= myscanner.nextInt(); //declare tip percent
        System.out.println("How many people are you going to split the bill with?"); //input for number of people
        int num_people = myscanner.nextInt(); //declare num_people

        double costPerPerson= (total*(1+tippercent/100))/num_people; //math to get cost per person
        int dollars=(int)costPerPerson; //set dollar values
        int dimes=(int)(costPerPerson * 10) % 10; //set 0.1 decimal holder
        int pennies=(int)(costPerPerson * 100) % 10; //set 0.01 decimal holder
        System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);//spit output

    }
}